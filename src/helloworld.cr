require "kemal"
require "kilt/slang"

# config
ENV["PORT"] ||= "8080"
Kemal.config.port = ENV["PORT"].to_i

# routes
get "/" do
  render "src/views/home.slang", "src/views/layouts/main.slang"
end

get "/hello" do
  name = "person I don't know the name off"
  render "src/views/hello.slang", "src/views/layouts/main.slang"
end

get "/hello/:name" do |env|
  name = env.params.url["name"]
  render "src/views/hello.slang", "src/views/layouts/main.slang"
end

# start
Kemal.run
