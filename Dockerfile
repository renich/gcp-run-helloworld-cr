# syntax=docker/dockerfile:1

# builder
## base
FROM crystallang/crystal:1.3-alpine AS builder

## install dependencies
WORKDIR /usr/src/app
COPY shard.yml shard.lock ./

## get code
COPY . ./

RUN shards install --production && crystal build --release --no-debug --static src/helloworld.cr


# app
## base
FROM alpine:latest

## install dependencies
RUN apk --no-cache add ca-certificates

## get src
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/helloworld ./

## run
CMD ["./helloworld"]
