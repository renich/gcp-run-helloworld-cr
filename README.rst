helloworld
==========
This is a minimal demo app for GCP's Run.

Deployment
----------
Once you have gcloud's CLI app setup, just configure your region:

.. code:: sh

    gcloud config set run/region <region>

You can find the regions `here <https://www.google.com/search?q=gcp+run+regions>`_.

.. note::
    If possible, pick a tier 1 pricing region. Those are cheaper (as of this writing).

And deploy:

.. code::

    gcloud run deploy

Reference
---------
* https://cloud.google.com/run/docs/quickstarts/build-and-deploy/ruby (pretty close)
